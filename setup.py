import glob
from distutils.core import setup

setup(
    name="maketemplate",
    version="0.1dev",
    packages=["maketemplate",],
    license="GPL",
    long_description=open("README.rst").read(),
    scripts=glob.glob('scripts/*.py')
)
