#!/usr/bin/env python
# encoding: utf-8


import os
import sys
import argparse
import glob
from subprocess import call
from maketemplate.generation import generatemakefile


# Makefile.template + *.symbol -> Makefile.in -> Makefile


class TemplateNotFound(Exception): pass


def findtemplate():
    """查找.template的文件"""
    # 从当前目录找起
    rest = os.getcwd()
    while rest != '/':
        lst = glob.glob(os.path.join(rest, '*.template'))
        if lst:
            return lst[0]
        rest, dir_ = os.path.split(rest)
    raise TemplateNotFound()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-T", dest="template", default=None)
    parser.add_argument("-d", "--debug", dest="debug", action="store_true",
                        help="pass to GNU Make")
    parser.add_argument('-C', dest='cwd', default='.')
    # parser.add_argument("-M", "--keep-intermediate-files", dest="gmonly",
    #                     action="store_true",
    #                     help="keep intermediate files")
    parser.add_argument('-I', '--intermediate', dest='intermediate',
                        default='Makefile.intermediate',
                        help='intermediate filename')
    parser.add_argument("-v", "--verbose", dest="verbose",
                        action="store_true")
    parser.add_argument("-S", dest="symbolfiles", nargs="+",
                        default=[])
    parser.add_argument("-o", dest="output", default="Makefile",
                        help="output filename, default to 'Makefile'")
    # TODO: 我想这里的问题是：在写Makefile的时候需要有一定的技巧，让不
    # 能并行的过程串行在一个Target中，问题我不知道什么行为是并行、什么
    # 是穿行的，就算我知道，对于维护的人员来说也要小心翼翼地，干脆把这
    # 个东西屏蔽掉，默认使用单任务的方式
    parser.add_argument("-j", "--jobs", dest="jobs", default=1, type=int,
                        help="default to 1")
    parser.add_argument("targets", nargs='*')
    args = parser.parse_args()
    if args.verbose:
        print "Generate", args.output
        print "Template", args.template
        print 'Working directory', args.cwd
        print "Symbols ", ', '.join(args.symbolfiles)
    os.chdir(args.cwd)
    if not args.template:
        # 在改变目录之后才查找template
        args.template = findtemplate()
    if not args.symbolfiles:
        # 在改变当前目录之后才进行glob操作
        args.symbolfiles = glob.glob('*.symbol')
    generatemakefile(args.template,
                     args.symbolfiles,
                     args.output)
    # if args.targets:
    additionalargs = ''
    if args.debug:
        additionalargs += '-d'
    selfcmd = [sys.argv[0], ' -j%d ' % args.jobs, additionalargs]
    cmd = "make %s -r -j%d %s MAKE='%s' MAKENAME=%s" %  \
        (additionalargs,
         args.jobs,
         ' '.join(args.targets),
         ' '.join(selfcmd),
         os.path.basename(sys.argv[0]))
    if args.verbose:
        print cmd
    r = call(cmd, shell=True)

    # 如果使用-j2以上的并发，就有可能会有别的进程先删除了这个文件，所以
    # 一个比较可靠的方法就是先使用1哥并发，当然为了提高编译速度，我还得
    # 想想其他的方法。如果下面的删除被执行了，会出现很奇怪的错误，就算
    # 我捕获了哪些错误，还是会被显示出来，一个比较稳但是不妥的办法就是
    # 不执行中间文件的删除

    # if args.gmonly:
    #     return
    # try:
    # os.remove(args.output)
    # os.remove(args.intermediate)
    # except OSError:
    #     pass

    sys.exit(r) ####


if __name__ == '__main__':
    main()
