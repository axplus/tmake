#!/usr/bin/env python
# encoding: utf-8
'''
2011-8-24

写了一个简单的工具来读取makefile.in文件，在makefile中嵌入python代码


当调用make.py debug32时:

  1. 调用configure.py
  2. 调用make debug32
'''


import sys
from StringIO import StringIO


__all__ = ['compilemakefile']


def compilemakefile(inputstream, outputstream):
    rtnvalue = compilemakefileobj(inputstream)
    outputstream.write(rtnvalue)
        
        
def compilemakefileobj(inputstream):
    """
    解释编译makefilein，把解释的结果输出到output中去
    """
    # 编译的intermediate文件中可能包含着对os, glob等的调用
    import os
    import glob
    # 解释%python..%中的内容
    sys.stdout = StringIO()
    inpython = False
    snippetlines = []
    for line in inputstream:
        line = line[:-1] if line.endswith('\n') else line
        if line == '%python':
            assert not inpython ####
            inpython = True
            snippetlines[:] = []
        elif inpython:
            # read until %
            if line.strip() == '%':
                inpython = False
                snippet = '\n'.join(snippetlines)
                exec snippet
            else:
                snippetlines.append(line)
        else:
            print line

    # 解释$(python ..)的内容
    # outputvalue = sys.stdout.getvalue()
    # sys.stdout.close()
    # sys.stdout = StringIO()
    # for line in outputvalue.split('\n'):
    #     rest = line
    #     snippets = []
    #     while True:
    #         try:
    #             pos = rest.index('$(python')
    #             endpos = pos + len('$(python')

    #             # 输出pos之前的所有字符
    #             snippets.append(rest[:pos])

    #             # 扫描这之后的每一个字符
    #             buf = []
    #             d = 0
    #             got = False
    #             for i, c in enumerate(rest[endpos:]):
    #                 # 结束掉$(python...)
    #                 if d == 0 and c == ')':
    #                     got = True
    #                     rest = rest[endpos+i+1:]
    #                     break

    #                 buf.append(c)
    #                 if c == '(':
    #                     d += 1
    #                     continue
    #                 if c == ')':
    #                     d -= 1
    #                     continue
    #             if got:
    #                 snippet = ''.join(buf)
    #                 r = eval(snippet)
    #                 snippets.append(r)
    #             else:
    #                 assert False, 'no close brace here' ####
    #         except ValueError: # not found
    #             snippets.append(rest)
    #             break
    # print ''.join(snippets)

    outputvalue = sys.stdout.getvalue()
    sys.stdout.close()
    sys.stdout = sys.__stdout__
    return outputvalue
