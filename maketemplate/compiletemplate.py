#!/usr/bin/env python
# encoding: utf-8
"""
2011-10-12

读取Makefile.template，对文档中的%..%进行替换。注意%..%中的内容会进行严
格的替换，包括空行、空格等等。这是与C PreProcess不同的地方。这个系统不
识别文字的文法之类，只识别格式。
"""


import sys
import os
from StringIO import StringIO


__all__ = ["compiletemplate"]


class Tokenizer(object):
    """只输出三种token"""

    EOF = 0
    SEGMENTID = 1
    OTHER = 2

    def __init__(self, inputstream):
        object.__init__(self)
        self._inputstream = inputstream
        self._nexttoken = None


    def peek(self):
        if not self._nexttoken:
            self._nexttoken = self._readtoken()
        return self._nexttoken


    def eat(self):
        if self._nexttoken:
            self._nexttoken = None
        else:
            self._readtoken()


    def _readtoken(self):
        """return ttype, tword"""
        lexstate = "START"
        buf = ''
        while True:
            ch = self._inputstream.read(1)
            if not ch and \
                    not buf:
                return Tokenizer.EOF, ''
            if lexstate == "START":
                buf += ch
                if ch == '%':
                    lexstate = "SEGMENTID"
                else:
                    lexstate = "OTHER"
            elif lexstate == "SEGMENTID":
                if ch == '%':
                    buf += ch
                    return Tokenizer.SEGMENTID, buf
                # 只有%\w+%的才认为是SEGMENTID
                elif ch.isalnum() or ch == '_':
                    buf += ch
                elif not ch:
                    # buf = '%' + buf
                    return Tokenizer.OTHER, buf
                else:
                    # buf = '%' + buf
                    buf += ch
                    lexstate = "OTHER"
            elif lexstate == "OTHER":
                if not ch:
                    return Tokenizer.OTHER, buf
                elif ch == '%':
                    self._inputstream.seek(-1, os.SEEK_CUR)
                    return Tokenizer.OTHER, buf
                else:
                    buf += ch


def compiletemplate(inputstream, outputstream, symboltable={}):
    compiletemplate_(Tokenizer(inputstream), outputstream, symboltable)


def compiletemplate_(tokenizer, outputstream, symboltable={}):
    while True:
        tt, tw = tokenizer.peek()
        if tt == Tokenizer.EOF:
            break
        elif tt == Tokenizer.SEGMENTID:
            # lookup symbol table for tw
            try:
                replacement = symboltable[tw[1:-1]]
                outputstream.write(replacement)
            except KeyError:
                # 没有定义这个模板的替换，跳过
                pass
        else:
            outputstream.write(tw)
        tokenizer.eat()
