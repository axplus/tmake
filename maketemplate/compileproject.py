#!/usr/bin/env python
# encoding: utf-8
"""
2011-10-12

合并Makefile.template和Makefile.project的工具脚本，只需要干一件事情：就
是编译Makefile.project -> 编译Makefile.template(使用之前编译结果展开)。

为了让这个东西简单了事，我使用LL无递归的语法。
"""


import sys


__all__ = ["compileproject"]


class Tokenizer(object):
    """把文件流转换成Token流"""
    # ANY = 0
    SEGMENTID = 1
    SEGMENTTEXT = 2
    EOF = 3

    def __init__(self, inputstream):
        object.__init__(self)
        self._inputstream = inputstream
        self._nexttoken = None


    def peek(self):
        """返回tokentype, tokenstring两个值的组合"""
        # 如果缓存中没有下一个Token
        if not self._nexttoken:
            # 读取下一个Token
            self._nexttoken = self._readtoken()
        return self._nexttoken


    def eat(self):
        # 标记下一个Token已经被吃掉了
        if not self._nexttoken:
            # 读取一个
            ttype, tword = self._readtoken()
            # if excepttokentype != ANY:
            #     assert ttype == excepttokentype
        # 标记没有下一个Token
        self._nexttoken = None


    def _readtoken(self):
        """从fileobj中读取下一个Token"""
        lexstate = "START"
        buf = ''
        while True:
            # 读取下一个字符
            ch = self._inputstream.read(1)
            if not ch:
                return Tokenizer.EOF, ""
            if lexstate == "START":
                if ch == '%':
                    lexstate = "SEGMENTID"
                elif ch == '{':
                    lexstate = "SEGMENTTEXT"
                elif ch == '#':
                    lexstate = "COMMENT"
            elif lexstate == "COMMENT":
                # 注释是直到#的位置，只有在SEGMENTTEXT之外的注释才有可
                # 能被识别到；跳过注释
                if ch == '\n':
                    lexstate = "START"
            elif lexstate == "SEGMENTID":
                if ch == '%':
                    return Tokenizer.SEGMENTID, buf
                else:
                    buf += ch
            elif lexstate == "SEGMENTTEXT":
                if ch == '}':
                    return Tokenizer.SEGMENTTEXT, buf
                else:
                    buf += ch
            else:
                assert False ####


# 这里要搞清楚，输出的并不是一个文字流，所以不应该使用outputstream


def compileproject(inputstream):
    return projectfile(Tokenizer(inputstream))


# projectfile ::= segments
def projectfile(tokenizer):
    return segments(tokenizer)


# segments ::= segment*
def segments(tokenizer):
    result = {}
    while True:
        tt, tw = tokenizer.peek()
        if tt == Tokenizer.EOF:
            return result
        sid, stext = segment(tokenizer)
        result[sid] = stext
    return result


# segment ::= SEGMENTID SEGMENTTEXT
def segment(tokenizer):
    tt, sid = tokenizer.peek()
    if tt == Tokenizer.SEGMENTID:
        tokenizer.eat()
        tt, stext = tokenizer.peek()
        assert tt == Tokenizer.SEGMENTTEXT ####
        tokenizer.eat()
        return sid, stext
