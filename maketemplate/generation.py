# encoding: utf-8


import compileproject
import compiletemplate
import compilemakefile


def compilesymbols(symbolfilenames):
    '''从一堆的symbolfilenames中得出所有symbol的合集'''
    bigsymboltable = {}
    for sfile in symbolfilenames:
        with file(sfile) as symbolinput:
            symboltable = compileproject.compileproject(symbolinput)
            bigsymboltable.update(symboltable)
    return bigsymboltable


def generatemakefile(templatefilename, symbolfilenames, outputfilename):
    """docstring for generatemakefile"""
    symboltable = compilesymbols(symbolfilenames)
    with file(templatefilename) as templateinput:
        with file('Makefile.intermediate', 'w') as outputstream:
            compiletemplate.compiletemplate(templateinput, outputstream,
                                            symboltable)
    with file('Makefile.intermediate') as inputstream:
        with file(outputfilename, 'w') as outputstream:
            compilemakefile.compilemakefile(inputstream, outputstream)


def generatetemplate(templatefilename, symbolfilenames, outputfilename):
    # 先编译所有symbolfiles，合成一张大的symboltable
    symboltable = compilesymbols(symbolfilenames)
    # 用这个symboltalbe来编译template
    with file(templatefilename) as templateinput:
        with file(outputfilename, 'w') as outputstream:
            r = compiletemplate.compiletemplate(templateinput, outputstream, symboltable)
