#


import unittest
import os


class CompileMakefileTestCase(unittest.TestCase):
    def test_one(self):
        import compilemakefile
        with file('test/data/Makefile.in.2') as inputstream:
            with file('test/gen/Makefile', 'w') as outputstream:
                compilemakefile.compilemakefile(inputstream, outputstream)
        self.assertTrue(os.path.isfile('test/gen/Makefile'))