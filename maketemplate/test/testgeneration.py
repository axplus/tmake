# encoding: utf-8


import unittest
import os


class GenerationTestCase(unittest.TestCase):
    """
    通过之前完成的两个部分内容，这个部分的就是把之前的两个编译过程合并
    起来，加上解释过程，就完工了。
    """

    def test_generatetemplate(self):
        """
        我想是这样的：

        compile(template="Makefile.template", symbolfiles=["Makefile.project"], output="Makefile.in")

        可以有多个symbolfile，但是template。
        """
        import generation
        generation.generatetemplate(templatefilename="test/data/Makefile.template",
                             symbolfilenames=["test/data/Makefile.symbol"],
                             outputfilename="test/gen/Makefile.in")
        self.assertTrue(os.path.exists("test/gen/Makefile.in"))

    def test_generatemakefile(self):
        import generation
        generation.generatemakefile(
        templatefilename="test/data/Makefile.template",
        symbolfilenames=["test/data/Makefile.symbol"],
        outputfilename='test/gen/Makefile')
