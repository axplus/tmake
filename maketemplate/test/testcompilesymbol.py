# encoding: utf-8
"""
2011-10-13

"""


import unittest


class CompileSymbolTestCase(unittest.TestCase):
    def test_call(self):
        import compileproject
        with file('test/data/Makefile.symbol') as inputstream:
            symboltable = compileproject.compileproject(inputstream)
            self.assertIn("GATHER_SOURCES", symboltable)
