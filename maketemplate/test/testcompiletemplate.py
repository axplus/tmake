# encoding: utf-8
"""
2011-10-13

"""


import unittest


class CompileTemplateTestCase(unittest.TestCase):
    def test_call(self):
        import compiletemplate
        with file('test/data/Makefile.template') as inputstream:
            with file('test/gen/Makefile.intermediate', 'w') as outputstream:
                compiletemplate.compiletemplate(inputstream, outputstream)

    def test_compilewithsymbol(self):
        import compileproject
        import compiletemplate
        with file('test/data/Makefile.symbol') as projectinput:
            with file('test/data/Makefile.template') as templateinput:
                with file('test/gen/Makefile.intermediate', 'w') as output:
                    st = compileproject.compileproject(projectinput)
                    outvalue = compiletemplate.compiletemplate(templateinput, output, st)
        